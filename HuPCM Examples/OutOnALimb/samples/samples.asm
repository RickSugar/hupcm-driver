
by4mattime_1    .incbin ".\samples\samples\by4mattime_1.5bt"
Untitled_2      .incbin ".\samples\samples\Untitled_2.5bt"
Untitled_3      .incbin ".\samples\samples\Untitled_3.5bt"
Untitled_4      .incbin ".\samples\samples\Untitled_4.5bt"
Untitled_5      .incbin ".\samples\samples\Untitled_5.5bt"
Untitled_6      .incbin ".\samples\samples\Untitled_6.5bt"
Untitled_7      .incbin ".\samples\samples\Untitled_7.5bt"
Untitled_8      .incbin ".\samples\samples\Untitled_8.5bt"
Untitled_9      .incbin ".\samples\samples\Untitled_9.5bt"
Untitled_A      .incbin ".\samples\samples\Untitled_A.5bt"
Untitled_B      .incbin ".\samples\samples\Untitled_B.5bt"
Untitled_C      .incbin ".\samples\samples\Untitled_C.5bt"
Untitled_D      .incbin ".\samples\samples\Untitled_D.5bt"



;//......................
;//Built tables for sample access
SampleAddr:
        .dw (by4mattime_1 & $1fff),(Untitled_2 & $1fff),(Untitled_3 & $1fff),(Untitled_4 & $1fff)
        .dw (Untitled_5 & $1fff),(Untitled_6 & $1fff),(Untitled_7 & $1fff)
        .dw (Untitled_8 & $1fff)
        .dw (Untitled_9 & $1fff),(Untitled_A & $1fff),(Untitled_B & $1fff),(Untitled_C & $1fff)
        .dw (Untitled_D & $1fff)

SampleBank:
        .db bank(by4mattime_1),bank(Untitled_2),bank(Untitled_3),bank(Untitled_4)
        .db bank(Untitled_5),bank(Untitled_6),bank(Untitled_7),bank(Untitled_8)
        .db bank(Untitled_9),bank(Untitled_A),bank(Untitled_B),bank(Untitled_C)
        .db bank(Untitled_D)