blank:          .incbin ".\samples\samples\blank.5bt"
subassbass_2    .incbin ".\samples\samples\subassbass_2.5bt"
shamkick_3      .incbin ".\samples\samples\shamkick_3.5bt"
shamsnare_4     .incbin ".\samples\samples\shamsnare_4.5bt"
rrraaigh_5      .incbin ".\samples\samples\rrraaigh_5.5bt"
discohat_6      .incbin ".\samples\samples\discohat_6.5bt"
discoshh_7      .incbin ".\samples\samples\discoshh_7.5bt"
clavichord_8    .incbin ".\samples\samples\clavichord_8.5bt"
brassitup_9     .incbin ".\samples\samples\brassitup_9.5bt"
_808clap_A      .incbin ".\samples\samples\808clap_A.5bt"
herwegonow_B    .incbin ".\samples\samples\herwegonow_B.5bt"
housepiano_C    .incbin ".\samples\samples\housepiano_C.5bt"
housepiano2_D   .incbin ".\samples\samples\housepiano2_D.5bt"
voxbell1_E      .incbin ".\samples\samples\voxbell1_E.5bt"
sonicboom_F     .incbin ".\samples\samples\sonicboom_F.5bt"
Untitled_10     .incbin ".\samples\samples\Untitled_10.5bt"
augh_11         .incbin ".\samples\samples\augh_11.5bt"
niceblip_12     .incbin ".\samples\samples\niceblip_12.5bt"

;//......................
;//Built tables for sample access
SampleAddr:
        .dw (blank & $1fff),(subassbass_2 & $1fff),(shamkick_3 & $1fff),(shamsnare_4 & $1fff)
        .dw (rrraaigh_5 & $1fff),(discohat_6 & $1fff),(discoshh_7 & $1fff),(clavichord_8 & $1fff)
        .dw (brassitup_9 & $1fff),(_808clap_A & $1fff),(herwegonow_B & $1fff),(housepiano_C & $1fff)
        .dw (housepiano2_D & $1fff),(voxbell1_E & $1fff),(sonicboom_F & $1fff),(Untitled_10 & $1fff)
        .dw (augh_11 & $1fff),(niceblip_12 & $1fff)
        

SampleBank:
        .db bank(blank),bank(subassbass_2),bank(shamkick_3),bank(shamsnare_4)
        .db bank(rrraaigh_5),bank(discohat_6),bank(discoshh_7),bank(clavichord_8)
        .db bank(brassitup_9),bank(_808clap_A),bank(herwegonow_B),bank(housepiano_C)
        .db bank(housepiano2_D),bank(voxbell1_E),bank(sonicboom_F),bank(Untitled_10)
        .db bank(augh_11),bank(niceblip_12)