
st01hallbrass_1 .incbin ".\samples\samples\st01hallbrass_1.5bt"
st01digdug_2    .incbin ".\samples\samples\st01digdug_2.5bt"
st01nice_3      .incbin ".\samples\samples\st01nice_3.5bt"
st01synbrass_4  .incbin ".\samples\samples\st01synbrass_4.5bt"
st01popsnare2_5 .incbin ".\samples\samples\st01popsnare2_5.5bt"
st01bassdrum2_6 .incbin ".\samples\samples\st01bassdrum2_6.5bt"
st01dxtom_7     .incbin ".\samples\samples\st01dxtom_7.5bt"
st01hihat2_8    .incbin ".\samples\samples\st01hihat2_8b.5bt"
blank           .incbin ".\samples\samples\blank.5bt"
st01blast_A     .incbin ".\samples\samples\st01blast_A.5bt"
st01hallbrass_B .incbin ".\samples\samples\st01hallbrass_B.5bt"
st01synbrass_C  .incbin ".\samples\samples\st01synbrass_C.5bt"
st01nice_D      .incbin ".\samples\samples\st01nice_D.5bt"
st01popsnare2_E .incbin ".\samples\samples\st01popsnare2_E.5bt"
st01blubzing_F  .incbin ".\samples\samples\st01blubzing_F.5bt"


;//......................
;//Built tables for sample access
SampleAddr:
        .dw (st01hallbrass_1 & $1fff),(st01digdug_2 & $1fff),(st01nice_3 & $1fff),(st01synbrass_4 & $1fff)
        .dw (st01popsnare2_5 & $1fff),(st01bassdrum2_6 & $1fff),(st01dxtom_7 & $1fff),(st01hihat2_8 & $1fff)
        .dw (blank & $1fff),(st01blast_A & $1fff),(st01hallbrass_B & $1fff),(st01synbrass_C & $1fff)
        .dw (st01nice_D & $1fff),(st01popsnare2_E & $1fff),(st01blubzing_F & $1fff)

SampleBank:
        .db bank(st01hallbrass_1),bank(st01digdug_2),bank(st01nice_3),bank(st01synbrass_4)
        .db bank(st01popsnare2_5),bank(st01bassdrum2_6),bank(st01dxtom_7),bank(st01hihat2_8)
        .db bank(blank),bank(st01blast_A),bank(st01hallbrass_B),bank(st01synbrass_C)
        .db bank(st01nice_D),bank(st01popsnare2_E),bank(st01blubzing_F)