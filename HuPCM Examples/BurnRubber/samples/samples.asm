vangeliskick:   .incbin ".\samples\samples\vangeliskick.5bt"
vangelissnare:  .incbin ".\samples\samples\vangelissnare.5bt"
beg:            .incbin ".\samples\samples\beg.5bt"
add:            .incbin ".\samples\samples\add.5bt"
gce:            .incbin ".\samples\samples\gce.5bt"
powerhjat:      .incbin ".\samples\samples\powerhjat.5bt"
XSyBaLoM1:      .incbin ".\samples\samples\XSyBaLoM1.5bt"
lead:           .incbin ".\samples\samples\lead.5bt"

SampleAddr: 
        .dw (vangeliskick & $1fff),(vangelissnare & $1fff),(beg & $1fff),(add & $1fff)
        .dw (gce & $1fff),(powerhjat & $1fff),(XSyBaLoM1 & $1fff),(lead & $1fff)
SampleBank:
        .db bank(vangeliskick),bank(vangelissnare),bank(beg),bank(add)
        .db bank(gce),bank(powerhjat),bank(XSyBaLoM1),bank(lead)
