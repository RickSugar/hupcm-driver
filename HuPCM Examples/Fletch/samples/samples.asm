
st17Fletch11_1  .incbin ".\samples\samples\st17Fletch11_1.5bt"
st17Fletch2_2   .incbin ".\samples\samples\st17Fletch2_2.5bt"
st17Fletch3_3   .incbin ".\samples\samples\st17Fletch3_3.5bt"
st17Fletch4_4   .incbin ".\samples\samples\st17Fletch4_4.5bt"
st17Fletch5_5   .incbin ".\samples\samples\st17Fletch5_5a.5bt"
st17Fletch6_6   .incbin ".\samples\samples\st17Fletch6_6.5bt"
st17Fletch9_7   .incbin ".\samples\samples\st17Fletch9_7.5bt"
st04MixSynth3_8 .incbin ".\samples\samples\st04MixSynth3_8.5bt"
st17Fletch8_9   .incbin ".\samples\samples\st17Fletch8_9.5bt"
st17Fletch10_A  .incbin ".\samples\samples\st17Fletch10_A.5bt"
blank           .incbin ".\samples\samples\blank.5bt"



;//......................
;//Built tables for sample access
SampleAddr:
        .dw (st17Fletch11_1 & $1fff),(st17Fletch2_2 & $1fff),(st17Fletch3_3 & $1fff),(st17Fletch4_4 & $1fff)
        .dw (st17Fletch5_5 & $1fff),(st17Fletch6_6 & $1fff),(st17Fletch9_7 & $1fff)
        .dw (st04MixSynth3_8 & $1fff)
        .dw (st17Fletch8_9 & $1fff),(st17Fletch10_A & $1fff),(blank & $1fff),(blank & $1fff)
        .dw (blank & $1fff),(blank & $1fff),(blank & $1fff)

SampleBank:
        .db bank(st17Fletch11_1),bank(st17Fletch2_2),bank(st17Fletch3_3),bank(st17Fletch4_4)
        .db bank(st17Fletch5_5),bank(st17Fletch6_6),bank(st17Fletch9_7),bank(st04MixSynth3_8)
        .db bank(st17Fletch8_9),bank(st17Fletch10_A),bank(blank),bank(blank)
        .db bank(blank),bank(blank),bank(blank)