
XSyBaLoM1_1     .incbin ".\samples\samples\XSyBaLoM1_1.5bt"
vangelissnare_2 .incbin ".\samples\samples\vangelissnare_2.5bt"
vangeliskick_3  .incbin ".\samples\samples\vangeliskick_3.5bt"
discohat_4      .incbin ".\samples\samples\discohat_4.5bt"
synkleadaa_5    .incbin ".\samples\samples\synkleadaa_5.5bt"
aea_6           .incbin ".\samples\samples\aea_6.5bt"
ceg_7           .incbin ".\samples\samples\ceg_7.5bt"
gdg_8           .incbin ".\samples\samples\gdg_8.5bt"
blank           .incbin ".\samples\samples\blank.5bt"
wap_A           .incbin ".\samples\samples\wap_A.5bt"
dga_B           .incbin ".\samples\samples\dga_B.5bt"
pstab2_C        .incbin ".\samples\samples\pstab2_C.5bt"


;//......................
;//Built tables for sample access
SampleAddr:
        .dw (XSyBaLoM1_1 & $1fff),(vangelissnare_2 & $1fff),(vangeliskick_3 & $1fff),(discohat_4 & $1fff)
        .dw (synkleadaa_5 & $1fff),(aea_6 & $1fff),(ceg_7 & $1fff),(gdg_8 & $1fff)
        .dw (blank & $1fff),(wap_A & $1fff),(dga_B & $1fff),(pstab2_C & $1fff)

SampleBank:
        .db bank(XSyBaLoM1_1),bank(vangelissnare_2),bank(vangeliskick_3),bank(discohat_4)
        .db bank(synkleadaa_5),bank(aea_6),bank(ceg_7),bank(gdg_8)
        .db bank(blank),bank(wap_A),bank(dga_B),bank(pstab2_C)